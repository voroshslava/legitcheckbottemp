import logging
import asyncio

from aiogram import Bot, Dispatcher

from src.handlers import router, bot

dp = Dispatcher()

async def main():
    dp.include_router(router=router)
    await dp.start_polling(bot)

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        print("Exit")