from aiogram.types import ReplyKeyboardMarkup, KeyboardButton, InlineKeyboardMarkup, InlineKeyboardButton


menu = ReplyKeyboardMarkup(keyboard=[
    [KeyboardButton(text="Legit check start by photo 📷".upper())],
    [KeyboardButton(text="Legit check start by link 🔗".upper())],
    [KeyboardButton(text="Моя подписка 🤑".upper()), KeyboardButton(text="О нас ✌".upper())]
], resize_keyboard=True)

photos_and_price = ReplyKeyboardMarkup(keyboard=[
    [KeyboardButton(text="Проверить ✅")],
    [KeyboardButton(text="Переснять 🔁")],
    [KeyboardButton(text="Отмена ❌")]
], resize_keyboard=True)

missing_el = InlineKeyboardMarkup(inline_keyboard=[
    [InlineKeyboardButton(text="Такого у меня нет 😟", callback_data='missing_el')]
])