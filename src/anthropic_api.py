import anthropic
import base64, httpx

class AnthropicAPI:
    def __init__(self, token: str) -> None:
        self.token = token
       
    def send_photo(self, image_url: str):
        answer = []
        
        token = self.token

        message = anthropic.Anthropic(api_key=token).messages.create(
            model="claude-3-opus-20240229",
            max_tokens=1024,
            messages=[
                {"role": "user", "content": "Привет, я пришлю тебе фото моих кроссовок. Проверь, пожалуйста, их по внешнему виду на подлинность."},
                {"role": "user", "content": {
                                                "type": "image",
                                                "source": {
                                                "type": "base64",
                                                "media_type": "image/jpeg",
                                                "data": base64.b64encode(httpx.get(image_url).content).decode("utf-8"),
                                                }
                                            },
                }
            ])
        
        for k,v in message["content"].values():
            if k == "text":
                answer.append(v)

        return answer[0]
