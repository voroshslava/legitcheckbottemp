from aiogram import Router, F, Bot
from aiogram.filters import CommandStart
from aiogram.types import Message, CallbackQuery
from aiogram.utils.media_group import MediaGroupBuilder
from aiogram.fsm.context import FSMContext
from aiogram.fsm.state import State, StatesGroup

import src.keyboards as kb
from src.anthropic_api import AnthropicAPI

TOKEN = "7177225297:AAHUnIdTiB7yVEc8lwGuspeMfYQvnTaVnTo"

bot = Bot(token=TOKEN)

router = Router()

class SendPhotosAndPriceFSM(StatesGroup):
    front = State()
    back = State()
    left = State()
    right = State()
    sole_photo = State()
    insole_photo = State()
    tag_photo = State()
    box_photo = State()
    price = State()

@router.message(CommandStart())
async def start(message: Message):

    await message.answer(f"Привет, {message.from_user.full_name}!👋\n\nЯ здесь, чтобы помочь вам проверить подлинность ваших модных находок. Если вы сомневаетесь в подлинности покупки или просто хотите подтвердить её оригинальность, отправьте мне фото и цену и я проведу проверку. Давайте начнем! ♥", reply_markup=kb.menu)

@router.message(F.text == "Legit check start by photo 📷".upper())
async def get_photos_from_all_sides(message: Message, state: FSMContext):
    await state.set_state(SendPhotosAndPriceFSM.front)

    await message.answer("Для проверки подлинности вашей обуви, пожалуйста, сфотографируйте каждую сторону: переднюю, заднюю, левую и правую. Убедись, что фотографии чёткие и детализированные, чтобы мы могли максимально точно провести анализ! 🧐💡")

    await message.answer("Пришлите фото спереди ⬆️")

@router.message(F.photo, SendPhotosAndPriceFSM.front)
async def get_sole_photo(message: Message, state: FSMContext):
    await state.update_data(front=message.photo)
    await state.set_state(SendPhotosAndPriceFSM.back)

    await message.answer("Пришлите фото сзади ⬇️")

@router.message(F.photo, SendPhotosAndPriceFSM.back)
async def get_sole_photo(message: Message, state: FSMContext):
    await state.update_data(back=message.photo)
    await state.set_state(SendPhotosAndPriceFSM.left)

    await message.answer("Пришлите фото слева ⬅️")

@router.message(F.photo, SendPhotosAndPriceFSM.left)
async def get_sole_photo(message: Message, state: FSMContext):
    await state.update_data(left=message.photo)
    await state.set_state(SendPhotosAndPriceFSM.right)

    await message.answer("Пришлите фото справа ➡️")

@router.message(F.photo, SendPhotosAndPriceFSM.right)
async def get_sole_photo(message: Message, state: FSMContext):
    await state.update_data(right=message.photo)
    await state.set_state(SendPhotosAndPriceFSM.sole_photo)

    await message.answer("Пришлите фото подошвы 👟")

@router.message(F.photo, SendPhotosAndPriceFSM.sole_photo)
async def get_insole_photo(message: Message, state: FSMContext):
    await state.update_data(sole_photo=message.photo)
    await state.set_state(SendPhotosAndPriceFSM.insole_photo)

    await message.answer("Пришлите фото стельки 👣")

@router.message(F.photo, SendPhotosAndPriceFSM.insole_photo)
async def get_tag_photo(message: Message, state: FSMContext):
    await state.update_data(insole_photo=message.photo)
    await state.set_state(SendPhotosAndPriceFSM.tag_photo)

    await message.answer("Пришлите фото бирки 🏷️", reply_markup=kb.missing_el)

@router.message(F.photo, SendPhotosAndPriceFSM.tag_photo)
async def get_box_photo(message: Message, state: FSMContext):
    await state.update_data(tag_photo=message.photo)
    await state.set_state(SendPhotosAndPriceFSM.box_photo)

    await message.answer("Пришлите фото стикера на коробке 📦", reply_markup=kb.missing_el)

@router.callback_query(F.data == 'missing_el')
async def exit(callback: CallbackQuery, state: FSMContext):
    current_state = await state.get_state()

    if current_state == SendPhotosAndPriceFSM.tag_photo.state:
        await state.set_state(SendPhotosAndPriceFSM.box_photo)
        await callback.message.answer("Пришлите фото стикера на коробке 📦", reply_markup=kb.missing_el)

    else:
        await state.set_state(SendPhotosAndPriceFSM.price)
        await callback.message.answer(f"Напишите стоимость вашей пары 💲")

@router.message(F.photo, SendPhotosAndPriceFSM.box_photo)
async def get_box_photo(message: Message, state: FSMContext):
    await state.update_data(box_photo=message.photo)
    await state.set_state(SendPhotosAndPriceFSM.price)

    await message.answer(f"Напишите стоимость вашей пары 💲")

@router.message(F.text, SendPhotosAndPriceFSM.price)
async def get_price(message: Message, state: FSMContext):
    await state.update_data(price=message.text)
    
    data = await state.get_data()

    price = data["price"]

    photos_ids = [v[-1].file_id for k,v in data.items() if k != "price"]

    media_group = MediaGroupBuilder()

    for photo_id in photos_ids:
        media_group.add_photo(media=photo_id)

    await bot.send_media_group(message.chat.id, media=media_group.build())
    await message.answer(text=f"Цена твоей пары 💸: {price}.00 RUB")
    await message.answer(text="Что делаем дальше? ☑️", reply_markup=kb.photos_and_price)

    await state.clear()

@router.message(F.text == "Проверить ✅")
async def resend_photos(message: Message):

    await message.answer("Ваш запрос обрабатывается, немного подождите ⏳")

@router.message(F.text == "Переснять 🔁")
async def resend_photos(message: Message, state: FSMContext):
    await state.set_state(SendPhotosAndPriceFSM.front)

    await message.answer("Хорошо, начнем заново, пришлите фото спереди ⬆️")

@router.message(F.text == "Отмена ❌")
async def resend_photos(message: Message):

    await message.answer("Вы отменили отправку фото вашей пары на проверку, начнем сначала? 🥵", reply_markup=kb.menu)

@router.message(F.text == "Legit check start by link 🔗".upper())
async def legit_check(message: Message):

    await message.reply("Пришлите ссылку на ту пару, которую вы хотите приобрести 🔍")

@router.message()
async def wrong_message(message: Message):
    
    await message.reply("Извините, я пока что не знаю таких команд 😒")

# @router.callback_query(F.data == 'send_photos_and_price')
# async def add_photos_and_price(callback: CallbackQuery):

#     await callback.message.answer("Your request is being processed ⏳")

# @router.callback_query(F.data == 'again')
# async def again(callback: CallbackQuery, state: FSMContext):

#     await state.set_state(SendPhotosAndPriceFSM.photos_from_all_sides)
#     await callback.message.answer()
        